# Тестовое задание Frontend разработчик для "Жилфонд"

[Тех.задание](https://docs.google.com/document/d/1duogz2g-vh-vvNzAkDRZu8fA1zr15bk6buCUhUVmrqs/edit)

[Макет](https://www.figma.com/design/31ULlO77yDmnFAf6ZmtFaI/%D0%A2%D0%B5%D1%81%D1%82%D0%BE%D0%B2%D0%BE%D0%B5-%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8-%22%D0%96%D0%B8%D0%BB%D1%84%D0%BE%D0%BD%D0%B4%22?node-id=1-63&t=VQKmxTJ2mqV8tk2K-0)

[Собранный проект](https://xevin.gitlab.io/search-user-jilfond/)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
