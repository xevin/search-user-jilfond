
function isNumber(val) {
  return !isNaN(val)
}

export default {
  namespaced: true,
  state: () => ({
    query: "",
    result: []
  }),
  getters: {
    parsedQuery(state) {
      if (!state.query.trim().length)
        return []

      return state.query.split(",")
        .filter(el => {
          let value = el.trim()
          return value.length
        })
    },
    buildFetchParams(state, getters) {
      return getters.parsedQuery
        .reduce((acc, value) => {
          value = value.trim()
          let query

          if (isNumber(value)) {
            query = `id=${Number(value)}`
          } else {
            query = `username=${value}`
          }

          return acc.length ? acc + "&" + query : acc + query
        }, "")
    },
  },
  mutations: {
    storeResult(state, payload) {
      state.result = payload
    },
    storeQuery(state, payload) {
      state.query = payload
    }
  },
  actions: {
    fetchResult({commit, getters}) {
      const url = "https://jsonplaceholder.typicode.com/users"

      return fetch(`${url}?${getters.buildFetchParams}`)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          commit("storeResult", data)
          return data
        })
    }
  }
}
